const express = require('express');
const os = require('os');

// Constants
const PORT = 4000;
const HOST = '0.0.0.0';
const CONTAINER = os.hostname();

// App
const app = express();
app.get('/api/micro-cuenta', (req, res) => {
  console.log('incoming request...')
  res.send(`Ambiente de Producción!\n container running: ${CONTAINER}`);
  console.log('delivered request... Ambiente de Productión!')
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);